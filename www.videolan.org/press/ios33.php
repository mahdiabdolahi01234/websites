<?php
   $title = "VideoLAN - Press Release - VLC for iOS 3.3";
   $lang = "en";
   $menu = array( "vlc" );
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<h1>VLC media player for iOS, iPadOS and tvOS version 3.3</h1>
<br />

<div class="longtext">
<div class="date">Paris, Mar 21 2022</div>

<p><b>We have exciting news to share. VLC media player for iPhone, iPod touch, iPad and Apple TV will receive a major update <a href="https://itunes.apple.com/app/id650377962">on the App Store</a> today!</b></p>

<p>It will be available free of charge in any country, requires iOS 9.0 or later and runs on iPhone 4S, iPad 2nd generation, 1st generation iPad mini and Apple TV HD or any later device.</p>

<?php image("vlc-ios/ipad-pro-iphone-13-iphone-4s.png" , "iPad Pro iPhone 13 iPhone 4s", "center-block img-responsive"); ?>

<p>This update adds a completely re-written video playback interface, adds support for browsing NFS and SFTP network shares, includes major speed improvements avoiding heating issues and greatly improves the local media library appearance especially for audio. Further, it enables a full black theme and access to all locally stored media including on external devices such as attached USB thumb drives. Of course, it adds various small fixes and many special interest features such as SAT>IP support.</p>

<?php image("vlc-ios/iphone13-landscape-library-black.png" , "iPhone 13 landscape library black", "center-block img-responsive"); ?>

<p>VLC for iOS is fully open-source. Its code is available online and is bi-licensed under both the Mozilla Public License Version 2 as well as the GNU General Public License Version 2 or later. The MPLv2 is applicable for distribution on the App Store.</p>

<p>We are happy to answer any questions you may have</p>

<h2>Links</h2>
<p><a href="https://itunes.apple.com/app/id650377962">VLC for iOS on the App Store</a></p>

<h2>Press Contact</h2>
<p>VideoLAN Press Team, <a href="mailto:press@videolan.org">press at videolan dot org</a></p>

<h2>Pictures</h2>

<?php image("vlc-ios/iphone4s-video-player.png" , "iPhone 4s video player", "center-block img-responsive"); ?>
<?php image("vlc-ios/ipad-pro-audio-library-popout.png" , "iPad pro audio library popout", "center-block img-responsive"); ?>
<?php image("vlc-ios/iphone5s-audio-library.png" , "iPhone 5s audio library", "center-block img-responsive"); ?>

</div>

<?php footer('$Id: ios33.php 6098 2010-05-26 23:50:46Z jb $'); ?>
